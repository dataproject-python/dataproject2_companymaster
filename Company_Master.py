import csv
import matplotlib.pyplot as plt

pathForMaharashtraDataFile = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT2/Maharashtra.csv"
pathForCityPincodesFile = "/home/hp/Desktop/MOUNTBLUE/DATA_PROJECTS/PROJECT2/CityPincodes.csv"

# 1st CODE


def plotAuthorizedCapHistogram(histRanges, xTemplates, authorizedCapData):

    # Creating Histogram using all of the above data

    plt.figure(figsize=(15, 8))
    plt.hist(authorizedCapData, histRanges, ec="black")
    plt.xticks(histRanges, xTemplates, rotation=90, size="small")
    plt.xlabel('RANGES')
    plt.title('HISTOGRAM OF AUTHORIZED CAP')
    plt.show()


def calculateAuthorizedCapHistogram(pathForMaharashtraDataFile):

    file = open(pathForMaharashtraDataFile, encoding='unicode_escape')
    csvreader = csv.DictReader(file)
    authorizedCapData = []
    for row in csvreader:
        authorizedCapValue = float(row['AUTHORIZED_CAP'])
        # First of all storing all of the Authorized Cap Data by converting it into first float and then int format
        authorizedCapData.append(authorizedCapValue)

    # Price ranges as per requirement in given problem
    histRanges = [0, 100001, 1000001, 10000001, 100000001]
    xTemplates = ['<= 1L', '1L to 10L', '10L to 1Cr',
                  '1Cr to 10Cr', '> 10Cr']        # Notations for x-axis

    file.close()

    return histRanges, xTemplates, authorizedCapData


def executeAuthorizedCapHistogram():

    histRanges, xTemplates, authorizedCapData = calculateAuthorizedCapHistogram(
        pathForMaharashtraDataFile)
    plotAuthorizedCapHistogram(histRanges, xTemplates, authorizedCapData)

# 2nd CODE


def plotCompanyRegistrationByYear(allDates, datesCounts):

    # Now simply create a bar using the year and count of entries data

    plt.figure(figsize=(30, 10))
    plt.bar(allDates, datesCounts)
    plt.xlabel('YEARS', fontsize=15)
    plt.ylabel('NUMBER OF REGISTRATIONS', fontsize=10)
    plt.title('COMPANY REGISTRATION BY YEAR', fontsize=15)
    plt.xticks(allDates, rotation=60)
    plt.show()


def calculateCompanyRegistrationByYear(pathForMaharashtraDataFile):

    file = open(pathForMaharashtraDataFile, encoding='unicode_escape')
    csvreader = csv.DictReader(file)

    # For storing all year values with there corresponding count values of registrations
    dataOfRegistration = {}
    for row in csvreader:
        date = str(row['DATE_OF_REGISTRATION'])        # First fetching date
        if date == 'NA':        # If it's NA then it means no proper date and year is mentioned, so we will skip this entry
            continue
        else:
            # Here we will fetch last string after splitting it using '-' for getting year value
            year = date.split('-')[2]
    # In old entries before 1900 date format is mentioned as 'yyyy-dd-mm' for that we will once confirm length of year is 4 or not
            if len(year) != 4:
                # If its not 4 then we will add the first stirg which will be the our correct year
                year = date.split('-')[0]
            if int(year) not in dataOfRegistration:
                dataOfRegistration[int(year)] = 1
            else:
                dataOfRegistration[int(year)] += 1

    # So using that logic we haven't skip any entry and stored all dates and years properly

    dataOfRegistration = dict(sorted(dataOfRegistration.items()))

    allDates = list(dataOfRegistration.keys())
    datesCounts = list(dataOfRegistration.values())

    file.close()

    return allDates, datesCounts


def executeCompanyRegistrationByYear():

    allDates, datesCounts = calculateCompanyRegistrationByYear(
        pathForMaharashtraDataFile)
    plotCompanyRegistrationByYear(allDates, datesCounts)

# 2nd CODE


def plotCompanyRegistrationByYear(allDates, datesCounts):

    # Now simply create a bar using the year and count of entries data

    plt.figure(figsize=(150, 30))
    plt.bar(allDates, datesCounts)
    plt.xlabel('YEARS', fontsize=70)
    plt.ylabel('NUMBER OF REGISTRATIONS', fontsize=70)
    plt.title('COMPANY REGISTRATION BY YEAR', fontsize=100)
    plt.xticks(allDates, rotation=60)
    plt.show()


def calculateCompanyRegistrationByYear(pathForMaharashtraDataFile):

    file = open(pathForMaharashtraDataFile, encoding='unicode_escape')
    csvreader = csv.DictReader(file)

    # For storing all year values with there corresponding count values of registrations
    dataOfRegistration = {}
    for row in csvreader:
        date = str(row['DATE_OF_REGISTRATION'])        # First fetching date
        if date == 'NA':        # If it's NA then it means no proper date and year is mentioned, so we will skip this entry
            continue
        else:
            # Here we will fetch last string after splitting it using '-' for getting year value
            year = date.split('-')[2]
    # In old entries before 1900 date format is mentioned as 'yyyy-dd-mm' for that we will once confirm length of year is 4 or not
            if len(year) != 4:
                # If its not 4 then we will add the first stirg which will be the our correct year
                year = date.split('-')[0]
            if int(year) not in dataOfRegistration:
                dataOfRegistration[int(year)] = 1
            else:
                dataOfRegistration[int(year)] += 1

    # So using that logic we haven't skip any entry and stored all dates and years properly

    dataOfRegistration = dict(sorted(dataOfRegistration.items()))

    allDates = list(dataOfRegistration.keys())
    datesCounts = list(dataOfRegistration.values())

    file.close()

    return allDates, datesCounts


def executeCompanyRegistrationByYear():

    allDates, datesCounts = calculateCompanyRegistrationByYear(
        pathForMaharashtraDataFile)
    plotCompanyRegistrationByYear(allDates, datesCounts)

# 3rd CODE


def plotCompanyRegistrationIn2015ByDistrict(distNames, distCounts):

    # Using all of the above data create a horizontal bar

    plt.figure(figsize=(15, 10))
    plt.barh(distNames, distCounts)
    plt.xlabel('NUMBER OF REGISTRATIONS')
    plt.ylabel('DISTRICT NAMES')
    plt.title('COMPANY REGISTRATION IN THE YEAR 2015 BY DISTRICT')
    plt.show()


def calculateCompanyRegistrationIn2015ByDistrict(pathForCityPincodesFile, pathForMaharashtraDataFile):

    file = open(pathForCityPincodesFile)
    csvreader = csv.DictReader(file)

    # For fetching and storing all available pincodes which we have of maharashtra
    citiesPincodeData = {}

    for row in csvreader:
        city = row['CITY']
        city = city.replace(' ', '')
        pincodeValue = int(row['PINCODE'])
        citiesPincodeData[pincodeValue] = city

    file.close()

    # Fetching the pincodes from address and counting them
    file = open(pathForMaharashtraDataFile, encoding='unicode_escape')
    csvreader = csv.DictReader(file)

    # For storing how many times a district found using different different pincodes
    districtsCount = {}
    # For storing all address which don't have proper pincode, so that we can process on this entries later
    unfoundData = []
    for row in csvreader:
        date = str(row['DATE_OF_REGISTRATION'])
        if date == 'NA':        # If year not available then skipping
            continue
        else:
            year = date.split('-')[2]
            if len(year) != 4:
                continue                # Skipping year value here because in our data old type of entries are present below 1900 and we have take only 2015 entries
            elif year == '2015':
                # Getting address
                address = row['Registered_Office_Address']
                # If pincode is present in address then its in last 6 letters of addresss, thats how fetching pincode
                pincode = address[-6:]
                if pincode.isnumeric():        # Checking if those values are numeric or not, which means its correct pincode or not
                    try:
                        # Cheking if pincode is available or not in our data
                        district = citiesPincodeData[int(pincode)]
                        if district not in districtsCount:        # If yes then just storing it and incrementing count of district
                            districtsCount[district] = 1
                        else:
                            districtsCount[district] += 1
                    except:                                      # If not found then just adding it to unfound
                        if int(pincode) not in unfoundData:
                            unfoundData.append(address)

    # Now finally we will process the data's who don't have pincode but still we can use it
    unfoundData = list(set(unfoundData))
    for a in unfoundData:
        flag = False
        for d in districtsCount:        # Checking if any of the districts which we have till is present in address or not
            if d.lower() in a.lower():
                flag = True
                break
        if flag:                        # If found any district name in address then we will just increment the counter of that district
            districtsCount[d] += 1

    # So using this logic of finding to which city this registration belongs without having pincode we utilized a lot of data

    # Now finally we will decide top cities only for displaying for a better overview of data
    displayData = {}
    for d in districtsCount:
        if districtsCount[d] >= 150:
            displayData[d] = districtsCount[d]

    distNames = list(displayData.keys())
    distCounts = list(displayData.values())

    file.close()

    return distNames, distCounts


def executeCompanyRegistrationIn2015ByDistrict():

    distNames, distCounts = calculateCompanyRegistrationIn2015ByDistrict(
        pathForCityPincodesFile, pathForMaharashtraDataFile)
    plotCompanyRegistrationIn2015ByDistrict(distNames, distCounts)

# # # 4th CODE - PENDING WORK AS QUESTION IS NOT CLEAR


# def registrationCountsOverYearAndPBA():
#     file = open(pathForMaharashtraDataFile, encoding='unicode_escape')
#     csvreader = csv.reader(file)
#     header = next(csvreader)

#     principalBusinessActivityTypes = {
#                                         'Other business activities':['other community','financial intermediation','extraterritorial organizations and bodies'],
#                                         'Unclassified' : ['social','education'],
#                                         'Agriculture, Forestry and Fishing':['agriculture','fishing','forestry','mining','oil'],
#                                         'Manufacture':['research and development','computer','production'],
#                                         'Construction' : ['real estate'],
#                                         'Services' : ['hotels','supply','transport','renting'],
#                                         'Wholesale & Retail Trade':['wholesale','retail']
#                                      }

#     allPricipalBusinessActivityCountsByYear = [[0, 0, 0, 0, 0, 0, 0] for i in range(5)]

#     for row in csvreader:
#         if row[6] == 'NA':
#             continue
#         else:
#             date = row[6]
#             year = date.split('-')[2]
#             if len(year) != 4:
#                 year = date.split('-')[0]
#             principalBusinessActivityData = {
#                                             'Other business activities' : 0,
#                                             'Unclassified' : 0,
#                                             'Agriculture, Forestry and Fishing' : 0,
#                                             'Manufacture' : 0,
#                                             'Construction' : 0,
#                                             'Services' : 0,
#                                             'Wholesale & Retail Trade' : 0,
#                                         }
#             flag = True
#             for i in principalBusinessActivityData:
#                 if i.lower() in row[11].lower():
#                     flag = False
#                     principalBusinessActivityData[i] += 1
#                     break
#             if flag:
#                 for i in principalBusinessActivityTypes['Other business activities'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Other business activities'] += 1
#                         break
#             if flag:
#                 for i in principalBusinessActivityTypes['Unclassified'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Unclassified'] += 1
#                         break
#             if flag:
#                 for i in principalBusinessActivityTypes['Agriculture, Forestry and Fishing'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Agriculture, Forestry and Fishing'] += 1
#                         break
#             if flag:
#                 for i in principalBusinessActivityTypes['Manufacture'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Manufacture'] += 1
#                         break
#             if flag:
#                 for i in principalBusinessActivityTypes['Construction'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Construction'] += 1
#                         break
#             if flag:
#                 for i in principalBusinessActivityTypes['Services'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Services'] += 1
#                         break
#             if flag:
#                 for i in principalBusinessActivityTypes['Wholesale & Retail Trade'] :
#                     if i in row[11].lower():
#                         flag=False
#                         principalBusinessActivityData['Wholesale & Retail Trade'] += 1
#                         break

#             year = int(year)
#             if year in range(1860, 1895):
#                 index = 0
#             elif year in range(1895, 1930):
#                 index = 1
#             elif year in range(1930, 1965):
#                 index = 2
#             elif year in range(1965, 2000):
#                 index = 3
#             elif year in range(2000, 2030):
#                 index = 4
#             for i in range(7):
#                 allPricipalBusinessActivityCountsByYear[index][i] += list(principalBusinessActivityData.values())[i]

#     x = list(principalBusinessActivityTypes.keys())
#     print(allPricipalBusinessActivityCountsByYear)

    # col1 = allPricipalBusinessActivityCountsByYear[0]
    # col2 = allPricipalBusinessActivityCountsByYear[1]
    # col3 = allPricipalBusinessActivityCountsByYear[2]
    # col4 = allPricipalBusinessActivityCountsByYear[3]
    # col5 = allPricipalBusinessActivityCountsByYear[4]

    # print(col1)

    # w = 0.1

    # bar1 = np.arange(0, len(col1))
    # bar2 = [i+w for i in bar1]
    # bar3 = [i+w for i in bar2]
    # bar4 = [i+w for i in bar3]
    # bar5 = [i+w for i in bar4]

    # plt.figure(figsize = (25, 10))
    # plt.bar(bar1, col1, w)
    # plt.bar(bar2, col2, w)
    # plt.bar(bar3, col3, w)
    # plt.bar(bar4, col4, w)
    # plt.bar(bar5, col5, w)
    # plt.show()

    # file.close()


# registrationCountsOverYearAndPBA()

executeAuthorizedCapHistogram()
executeCompanyRegistrationByYear()
executeCompanyRegistrationIn2015ByDistrict()
